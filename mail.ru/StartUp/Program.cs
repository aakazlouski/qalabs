﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GitHubAutomation.Tests;
namespace StartUp
{
    class Program
    {
        static void Main(string[] args)
        {
            SmokeTests test = new SmokeTests();
            test.Init();
            test.OneCanLoginMail();
            test.OneCanUseSearch();
            test.OneCanLogoutMail();
            test.Cleanup();
            Console.WriteLine("TestCompleted");
        }
    }
}
