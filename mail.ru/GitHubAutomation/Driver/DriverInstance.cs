﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace GitHubAutomation.Driver
{
    public class DriverInstance
    {
        private static IWebDriver _driver;

        private DriverInstance() { }

        public static IWebDriver GetInstance()
        {
            if (_driver == null)
            {
                _driver = new FirefoxDriver();
                _driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
                _driver.Manage().Window.Maximize();
            }
            return _driver;
        }

        public static void CloseBrowser()
        {
            _driver.Close();
            _driver = null;
        }
    }
}
