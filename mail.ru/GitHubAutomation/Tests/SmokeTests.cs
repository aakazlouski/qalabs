﻿using NUnit.Framework;

namespace GitHubAutomation.Tests
{
    [TestFixture]
    public class SmokeTests
    {
        private Steps.Steps steps = new Steps.Steps();
        private const string USERNAME = "seleniumtest009";
        private const string PASSWORD = "Selenium09!";
        private const string SearchString = "Selenium";
        [SetUp]
        public void Init()
        {
            steps.InitBrowser();
        }

        [TearDown]
        public void Cleanup()
        {
            steps.CloseBrowser();
        }

        [Test]
        public void OneCanLoginMail()
        {
            steps.LoginMail(USERNAME, PASSWORD);
            Assert.True(steps.IsLoggedIn(USERNAME));
        }
        [Test]
        public void OneCanLogoutMail()
        {
            steps.LogoutMail(USERNAME, PASSWORD);
            Assert.True(steps.IsLoggedOut());
        }
        [Test]
        public void OneCanUseSearch()
        {
            steps.SearchWorking(SearchString);
            Assert.True(steps.IsSearchWorking());
        }


    }
}
