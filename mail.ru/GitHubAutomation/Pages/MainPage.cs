﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace GitHubAutomation.Pages
{
    public class MainPage
    {
        private const string BASE_URL = "https://mail.ru";

        [FindsBy(How = How.Id, Using = "mailbox__login")]
        private IWebElement inputLogin;

        [FindsBy(How = How.Id, Using = "mailbox__password")]
        private IWebElement inputPassword;

        [FindsBy(How = How.Id, Using = "mailbox__auth__button")]
        private IWebElement buttonSubmit;

        [FindsBy(How = How.Id, Using = "PH_user-email")]
        private IWebElement linkLoggedInUser;

        [FindsBy(How = How.Id, Using = "q")]
        private IWebElement searchInput;

        [FindsBy(How = How.Id, Using = "search__button__wrapper__field")]
        private IWebElement searchInputButton;

        [FindsBy(How = How.Id, Using = "js-result")]
        private IWebElement searchResultMarker;

        [FindsBy(How = How.Id, Using = "PH_authMenu_button")]
        private IWebElement userManagementDropDown;

        [FindsBy(How = How.ClassName, Using = "js-logout")]
        private IWebElement logoutButton;
      
        private IWebDriver driver;

        public MainPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(this.driver, this);
        }

        public void OpenPage()
        {
            driver.Navigate().GoToUrl(BASE_URL);
            Console.WriteLine("Main Page opened");
        }

        public void Login(string username, string password)
        {
            inputLogin.SendKeys(username);
            inputPassword.SendKeys(password);
            buttonSubmit.Click();
        }
        public void Logout()
        {
            userManagementDropDown.Click();
            logoutButton.Click();
        }
        public void Search(string input)
        {
            searchInput.SendKeys(input);
            searchInputButton.Click();
        }
        public string GetLoggedInUserName()
        {
            return linkLoggedInUser.Text;
        }
        public bool SearchCompleted()
        {
            return searchResultMarker.Displayed;
        }
        public bool LoginButtonExist()
        {
            return buttonSubmit.Displayed;
        }
        
    }
}
