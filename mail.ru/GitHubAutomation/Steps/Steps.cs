﻿using OpenQA.Selenium;

namespace GitHubAutomation.Steps
{
    public class Steps
    {
        IWebDriver driver;

        public void InitBrowser()
        {
            driver = Driver.DriverInstance.GetInstance();
        }

        public void CloseBrowser()
        {
            Driver.DriverInstance.CloseBrowser();
        }

        public void LoginMail(string username, string password)
        {
            Pages.MainPage loginPage = new Pages.MainPage(driver);
            loginPage.OpenPage();
            loginPage.Login(username, password);
        }
        public void LogoutMail(string username, string password)
        {
             Pages.MainPage loginPage = new Pages.MainPage(driver);
            loginPage.OpenPage();
            loginPage.Login(username, password);
            loginPage.Logout();
        }
        public void SearchWorking(string input)
        {
            Pages.MainPage loginPage = new Pages.MainPage(driver);
            loginPage.OpenPage();
            loginPage.Search(input);
        }
        public bool IsLoggedIn(string username)
        {
            Pages.MainPage loginPage = new Pages.MainPage(driver);
            return (loginPage.GetLoggedInUserName().Trim().ToLower().Equals(username + "@mail.ru"));
        }
        public bool IsSearchWorking()
        {
            Pages.MainPage loginPage = new Pages.MainPage(driver);
            return loginPage.SearchCompleted();
        }
        public bool IsLoggedOut()
        {
            Pages.MainPage loginPage = new Pages.MainPage(driver);
            return  loginPage.LoginButtonExist();
        }
    }
}
