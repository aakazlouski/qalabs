﻿using System;
using TextEditor.Models;

namespace TextEditor.Services
{
    public static class TextService
    {
        public static Text ChangeSymbolInEveryWordInText(Text text, int position, char symbol)
        {
            foreach (var sentence in text.Sentences)
            {
                foreach (var word in sentence.Words)
                {
                    if (word.GetValue().Length > position - 1)
                        word.SetValue(word.GetValue().Remove(position - 1, 1).Insert(position - 1, symbol.ToString()));
                }
            }
            return text;
        }

        public static void WriteText(Text text)
        {
            var textToCout = string.Empty;
            foreach (var sentence in text.Sentences)
            {
                foreach (var word in sentence.Words)
                {
                    textToCout += ' ';
                    textToCout += word.GetValue();
                }
                textToCout += '.';
            }
            Console.WriteLine(textToCout);
        }
    }
}