﻿using System.Collections.Generic;

namespace TextEditor.Models
{
    public class Sentence
    {
        private string SentenceValue { get; set; }

        public List<Word> Words { get; set; }

        public Sentence(string text)
        {
            SentenceValue = text;
            Words = new List<Word>();
            ParseToWords();
        }

        public void ParseToWords()
        {
            var words = SentenceValue.Split(' ');
            foreach (var wordValue in words)
            {
                var word = new Word(wordValue);
                if (wordValue.Length > 0)
                    Words.Add(word);
            }
        }
    }
}