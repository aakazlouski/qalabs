﻿namespace TextEditor.Models
{
    public class Word
    {
        private string WordText { get; set; }

        public Word(string text)
        {
            WordText = text;
        }

        public string GetValue()
        {
            return WordText;
        }

        public void SetValue(string text)
        {
            WordText = text;
        }
    }
}