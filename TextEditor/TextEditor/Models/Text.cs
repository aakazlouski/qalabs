﻿using System.Collections.Generic;

namespace TextEditor.Models
{
    public class Text
    {
        private string TextValue { get; set; }

        public List<Sentence> Sentences { get; set; }

        public Text()
        {
        }

        public Text(string text)
        {
            TextValue = text;
            Sentences = new List<Sentence>();
            ParseToSentences();
        }

        public void ParseToSentences()
        {
            var separatedText = TextValue.Split('.');
            foreach (var sentenceText in separatedText)
            {
                if (sentenceText.Length > 0)
                    Sentences.Add(new Sentence(sentenceText));
            }
        }
    }
}