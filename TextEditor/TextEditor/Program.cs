﻿using System;
using TextEditor.Models;
using TextEditor.Services;

public class Program
{
    public static void Main()
    {
        Console.WriteLine("input text (sentences splited by dot,wordds by spaces)");
        var inputText = Console.ReadLine();
        Console.WriteLine("input # of char");
        int position = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("input symbol of char");
        char symbol = Convert.ToChar(Console.ReadLine());
        var sourceText = new Text(inputText);
        Text resultText = TextService.ChangeSymbolInEveryWordInText(sourceText, position, symbol);
        TextService.WriteText(resultText);
        Console.ReadLine();
    }
}